#!/bin/bash
echo "update_index_html - START"
set -e
set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

# replace the base href with BASE_URL environment variable if set, with / otherwise
sed -i "s#<base href=\"\/\">#<base href=\"${BASE_URL:-/}\">#g" /app/index.html
sed -i "s#<meta name=\"apiBaseUrl\" content=\".*\">#<meta name=\"apiBaseUrl\" content=\"${BASE_URL:-/}api\">#g" /app/index.html

echo "===================="
cat /app/index.html
echo "===================="

# nginx -g "daemon off;"

. /liblog.sh
. /libnginx.sh

info "Starting nginx... "

exec "$NGINX_BASEDIR/sbin/nginx" -c "$NGINX_CONFDIR/nginx.conf" -g "daemon off;"
