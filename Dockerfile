FROM bitnami/nginx:latest

# The following environment variable is an option to configure the front-end application
# ENV BASE_URL Optional variable to set the url context under which the application is deployed (e.g: /vrum/).
#              If not set, defaults to /.

# Service configuration using the entrypoint.
# We replace the <base href> in the index.html and start nginx.
COPY update_index_html.bash /update_index_html.bash
USER root
RUN mkdir /log
RUN chmod ug+rw /log
RUN chmod a+rx /update_index_html.bash

RUN mkdir /opt/bitnami/nginx/tmp
RUN chmod ug+rw /opt/bitnami/nginx/tmp

USER 1001

CMD ["/update_index_html.bash"]