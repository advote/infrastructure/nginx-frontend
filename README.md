# Base nginx image for server angular frontend resources [![build status](https://gitlab.com/chvote-2.0/infra/nginx-frontend/badges/master/build.svg)](https://gitlab.com/chvote-2.0/infra/nginx-frontend/commits/master)

## Content
* nginx: latest
* script file used to update the root html page of the application with the right base urls for
the frontend resources and for the backend APIs.